#!/usr/bin/env python3

from pyspark import SparkContext, SparkConf
from pyspark.streaming import StreamingContext
from hdfs import Config
import subprocess
import time
import os
import hyperloglog

client = Config().get_client()
nn_address = subprocess.check_output('hdfs getconf -confKey dfs.namenode.http-address', shell=True).strip().decode("utf-8")

sc = SparkContext(master='yarn-client')

# Preparing base RDD with the input data
DATA_PATH = "/data/realtime/uids"

batches = [sc.textFile(os.path.join(*[nn_address, DATA_PATH, path])) for path in client.list(DATA_PATH)[:3]]

finished = False

def print_always(rdd):
    global finished
    counters = rdd.collect()[0][1]

    if counters["is_finished"]:
        finished = True

    print("Windows: {}; Firefox: {}; Iphone: {}".format(len(counters['windows']), len(counters['firefox']), len(counters['iphone'])))

def counter(new_values, current_counter):
    if current_counter is None:
        current_counter = {'windows': hyperloglog.HyperLogLog(0.01), 'firefox': hyperloglog.HyperLogLog(0.01),
                           'iphone': hyperloglog.HyperLogLog(0.01), "is_finished": False}

    if not new_values:
        current_counter["is_finished"] = True
        print('Finished!')
        return (current_counter or 0)
    else:
        for v in new_values:
            # print(": " + v[:60])
            uid, browser_info = v.split('\t')
            browser_info = browser_info.lower()
            # m = re.match(r"\S+\t\S+ \((\S+)[\s\;]", v)
            # if m:
            if ('windows' in browser_info):
                current_counter['windows'].add(uid)

            if ('firefox' in browser_info):
                current_counter['firefox'].add(uid)

            if ('iphone' in browser_info):
                current_counter['iphone'].add(uid)

        return current_counter

# Creating QueueStream to emulate realtime data generating
BATCH_TIMEOUT = 2 # Timeout between batch generation
ssc = StreamingContext(sc, BATCH_TIMEOUT)
dstream = ssc.queueStream(rdds=batches)

result = dstream.map(lambda row: ('row', row)).updateStateByKey(counter).foreachRDD(print_always)

ssc.checkpoint('./checkpoint_{}'.format(time.strftime("%Y_%m_%d_%H_%M_%s", time.gmtime())))  # save accumulated results

ssc.start()
while not finished:
    pass
ssc.stop()